const { exec } = require("child_process");

const execCommand = (command) => {
  return new Promise((resolve, reject) => {
    if (!command) return reject({ message: "Empty command" });
    exec(command, (error, stdout, stderr) => {
      if (error) return reject({ message: stderr });
      resolve(stdout);
    });
  });
};

module.exports = { execCommand };
