const axios = require("axios");

const token = process.env.TOKEN;
if (!token) {
  console.log("No token provided");
  process.exit(1);
}

const getTorrents = async () => {
  try {
    await axios.get("http://localhost:5000/api/torrents", {
      headers: {
        "x-access-token": token,
      },
    });
  } catch (error) {
    console.log(error.response.data);
    process.exit(1);
  }
};

setInterval(getTorrents, 10);
