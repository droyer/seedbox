const xmlrpc = require("xmlrpc");
const path = require("path");

const call = (client, methodName, ...args) => {
  return new Promise((resolve, reject) => {
    client.methodCall(methodName, args, (error, value) => {
      if (value != null) resolve(value);
      if (error != null) {
        reject(new Error("xmlrpc error"));
      }
    });
  });
};

const createClient = (username) => {
  return xmlrpc.createClient({
    url: `http://127.0.0.1/${username.toUpperCase()}`,
  });
};

const getTorrents = async (client) => {
  return await call(client, "download_list");
};

const newFileTorrent = async (client, file) => {
  const fullPath = path.resolve(file);
  return await call(client, "load.start", "", fullPath);
};

const newMagnetTorrent = async (client, magnet) => {
  return await call(client, "load.start", "", magnet);
};

const getTorrentName = async (client, hash) => {
  return await call(client, "d.name", hash);
};

const getTorrentSize = async (client, hash) => {
  return await call(client, "d.size_bytes", hash);
};

const getTorrentDownload = async (client, hash) => {
  return await call(client, "d.completed_bytes", hash);
};

const getTorrentSent = async (client, hash) => {
  return await call(client, "d.up.total", hash);
};

const getTorrentReception = async (client, hash) => {
  return await call(client, "d.down.rate", hash);
};

const getTorrentPath = async (client, hash) => {
  return await call(client, "d.base_path", hash);
};

const deleteTorrent = async (client, hash) => {
  return await call(client, "d.erase", hash);
};

module.exports = {
  createClient,
  getTorrents,
  newFileTorrent,
  newMagnetTorrent,
  getTorrentName,
  getTorrentSize,
  getTorrentDownload,
  getTorrentSent,
  getTorrentReception,
  getTorrentPath,
  deleteTorrent,
};
