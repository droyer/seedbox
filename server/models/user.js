const mongoose = require("../utils/mongoose");
const { Torrent } = require("./torrent");

const User = mongoose.model("User", {
  name: {
    type: String,
    required: true,
  },
  pid: {
    type: Number,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    required: true,
  },
  password: {
    type: String,
  },
  torrents: [{ type: Torrent.schema }],
});

module.exports = { User };
