const { User } = require("../models/user");
const fs = require("fs").promises;

const start = async () => {
  await require("../startup/db")();

  const users = await User.find({});

  for (const user of users)
    await fs.writeFile(`/users/${user.name}`, user.pid + "\n");

  process.exit(0);
};

start();
