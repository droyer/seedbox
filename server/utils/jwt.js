const jwt = require("jsonwebtoken");

const secret = process.env.JWT_SECRET;

const generate = (id) => {
  const options = {};
  if (process.env.NODE_ENV === "production") {
    options.expiresIn = "24h";
  }
  return jwt.sign({ id }, secret, options);
};

const verify = (token) => {
  return jwt.verify(token, secret);
};

module.exports = {
  generate,
  verify,
};
