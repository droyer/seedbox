FROM node:14.15.1-buster-slim

WORKDIR /app

RUN apt-get update && apt-get upgrade -y \
 && apt-get install -y \
    psmisc \
    screen \
    nginx \
    rtorrent \
 && rm /etc/nginx/sites-enabled/* \
 && rm -rf /tmp/*

COPY server .
COPY rootfs /

RUN npm install --production

EXPOSE 80

CMD ["run.sh"]