var multer = require("multer");
const { v1: uuid } = require("uuid");
const HttpError = require("../utils/http-error");

const upload = multer({
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, "/tmp");
    },
    filename: (req, file, cb) => {
      cb(null, `${uuid()}.torrent`);
    },
  }),
  fileFilter: (req, file, cb) => {
    const isValid = file.mimetype === "application/x-bittorrent" || "application/octet-stream";
    const error = isValid ? null : new HttpError("Invalid mime type", 400);
    cb(error, isValid);
  },
});

module.exports = upload;
