const express = require("express");
const { Torrent } = require("../models/torrent");
const auth = require("../middlewares/auth");
const upload = require("../middlewares/upload");
const xmlrpc = require("../utils/xmlrpc");
const HttpError = require("../utils/http-error");
const fs = require("fs").promises;
const getMutex = require("../utils/mutex");

const router = express.Router();

router.get("/", auth, async (req, res) => {
  const { user } = req;
  const { torrents } = user;

  const mutex = getMutex(user.id);
  if (mutex.isLocked()) return res.json({ torrents });

  const release = await mutex.acquire();

  const client = xmlrpc.createClient(user.name);

  const hashes = await xmlrpc.getTorrents(client);
  for (const hash of hashes) {
    let torrent = torrents.find((t) => t.hash === hash);
    if (!torrent) {
      torrent = new Torrent({ hash });
      torrents.push(torrent);
      await user.save();
    }
  }

  for (const i in torrents) {
    const torrent = torrents[i];

    if (!hashes.includes(torrent.hash)) {
      torrents.splice(i, 1);
      continue;
    }

    if (!torrent.name || torrent.name.split(".").pop() === "meta")
      torrent.name = await xmlrpc.getTorrentName(client, torrent.hash);
    if (!torrent.size || torrent.download > torrent.size)
      torrent.size = await xmlrpc.getTorrentSize(client, torrent.hash);
    if (torrent.download !== torrent.size)
      torrent.download = await xmlrpc.getTorrentDownload(client, torrent.hash);
    torrent.sent = await xmlrpc.getTorrentSent(client, torrent.hash);
    torrent.reception =
      torrent.download !== torrent.size
        ? await xmlrpc.getTorrentReception(client, torrent.hash)
        : 0;
  }

  await user.save();

  release();

  res.json({ torrents });
});

const newTorrent = async (client, user) => {
  const hashes = await xmlrpc.getTorrents(client);
  const hash = hashes[hashes.length - 1];
  if (!hash) throw new Error();

  if (await user.torrents.find((item) => item.hash === hash)) return {};

  const torrent = new Torrent({ hash });
  user.torrents.push(torrent);
  await user.save();

  return torrent;
};

router.post("/file", [auth, upload.array("torrents")], async (req, res) => {
  const { user, files } = req;

  const mutex = getMutex(user.id);
  const release = await mutex.acquire();

  const torrents = [];
  if (files) {
    const client = xmlrpc.createClient(user.name);
    for (const file of files) {
      await xmlrpc.newFileTorrent(client, file.path);
      torrents.push(await newTorrent(client, user));
    }
  }

  release();

  res.status(201).json({ torrents });
});

router.post("/magnet", auth, async (req, res) => {
  const { user } = req;
  const { magnet } = req.body;

  const mutex = getMutex(user.id);
  const release = await mutex.acquire();

  const client = xmlrpc.createClient(user.name);
  await xmlrpc.newMagnetTorrent(client, magnet);
  const torrent = await newTorrent(client, user);

  release();

  res.status(201).json({ torrent });
});

router.delete("/:hash", auth, async (req, res) => {
  const { user } = req;
  const { torrents } = user;
  const { hash } = req.params;

  const mutex = getMutex(user.id);
  const release = await mutex.acquire();

  const torrent = torrents.find((item) => item.hash === hash);
  if (!torrent) throw new HttpError("Torrent not found", 404);

  user.torrents = torrents.filter((item) => item.hash !== hash);
  await user.save();

  const client = xmlrpc.createClient(user.name);

  const path = await xmlrpc.getTorrentPath(client, hash);
  await fs.rmdir(path, { recursive: true });

  await xmlrpc.deleteTorrent(client, hash);

  release();

  res.json();
});

module.exports = router;
