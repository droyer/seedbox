const mongoose = require("mongoose");

const model = (name, object) => {
  const schema = new mongoose.Schema(object);

  schema.set("toJSON", {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
      delete ret._id;
    },
  });

  return mongoose.model(name, schema);
};

module.exports = { model };
