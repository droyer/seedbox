const express = require("express");
const auth = require("../middlewares/auth");
const HttpError = require("../utils/http-error");
const fs = require("fs").promises;
const path = require("path");

const router = express.Router();

router.get("/", auth, async (req, res) => {
  const { path: pathQuery, file } = req.query;
  if (!pathQuery && !file) {
    throw new HttpError("No path and file query", 400);
  }

  const { user } = req;
  const rootFolder = `/home/${user.name}/torrents/`;

  const httpError = new HttpError("Path not exists", 400);

  const link = pathQuery ? pathQuery : file;
  const target = path.join(rootFolder, link);
  if (!target.startsWith(rootFolder)) throw httpError;

  if (pathQuery) {
    const folders = [];
    const files = [];

    try {
      const targetStat = await fs.lstat(target);

      if (targetStat.isDirectory()) {
        const list = await fs.readdir(target);
        for (const item of list) {
          const stat = await fs.lstat(path.join(target, item));
          if (stat.isFile()) {
            const extension = item.substring(item.lastIndexOf(".") + 1);
            if (extension != "meta") files.push(item);
          } else if (stat.isDirectory()) folders.push(item);
        }
      } else if (targetStat.isFile()) {
        return res.json({ file: pathQuery });
      }
    } catch (error) {
      throw httpError;
    }

    return res.json({ folders, files });
  }

  res.download(target);
});

module.exports = router;
