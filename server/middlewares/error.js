const getMutex = require("../utils/mutex");
const fs = require("fs").promises;

module.exports = async (err, req, res, next) => {
  const user = req.user;
  if (user) {
    const mutex = getMutex(user.id);
    mutex.release();
  }

  const { file } = req;
  if (file) await fs.unlink(file.path);

  if (res.headerSend) next(err);
  res.status(err.code || 500);
  if (!err.code) console.log(err);
  res.json({ message: err.message || "Something failed." });
};
