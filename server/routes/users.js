const express = require("express");
const { User } = require("../models/user");
const bcrypt = require("bcrypt");
const HttpError = require("../utils/http-error");
const _ = require("lodash");
const jwt = require("../utils/jwt");
const admin = require("../middlewares/admin");
const validate = require("../utils/validate");
const auth = require("../middlewares/auth");
const { execCommand } = require("../utils/utility");

const router = express.Router();

router.get("/", admin, async (req, res) => {
  const users = await User.find({}, "-password");
  res.json({ users });
});

router.post("/", async (req, res) => {
  const len = await User.countDocuments();
  if (len > 0) {
    await admin(req, res, null);
  }

  await validate(req.body, "name", "pid");

  const { name, pid } = req.body;

  if (await User.findOne({ name }))
    throw new HttpError("User already exists", 400);

  if (await User.findOne({ pid }))
    throw new HttpError("Pid already exists", 400);

  let user = new User(req.body);
  user.isAdmin = len == 0;

  await user.save();

  await execCommand(`create-user.sh ${name} ${pid}`);

  res.status(201).json();
});

router.post("/:id", async (req, res) => {
  const id = req.params.id;
  let user = await User.findById(id);

  const error = new HttpError("User not found", 404);
  if (!user || user.password) throw error;

  const { password } = req.body;
  await validate(req.body, "password");

  if (password.length < 3) throw new HttpError("Bad password");

  user.password = await bcrypt.hash(password, 12);
  await user.save();

  const token = jwt.generate(user.id);
  res.json({ token, user: _.pick(user, ["name", "isAdmin"]) });
});

router.get("/me", auth, async (req, res) => {
  res.json({ user: _.pick(req.user, "name", "isAdmin") });
});

module.exports = router;
