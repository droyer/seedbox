const auth = require("../middlewares/auth");
const HttpError = require("../utils/http-error");

module.exports = async (req, res, next) => {
  await auth(req, res, null);

  if (!req.user.isAdmin) throw new HttpError("Access denied", 403);

  next && next();
};
