#!/bin/bash

user=$1
pid=$2

useradd -M $user -u $pid
mkdir -p /home/$user/{torrents,.session}
chown $user:$user /home/$user/{torrents,.session}

file=/etc/init.d/$user-rtorrent
sed "s/<username>/$user/g" /usr/share/seedbox-templates/username-rtorrent > $file
chmod 755 $file

sed "s/<username>/$user/g" /usr/share/seedbox-templates/.rtorrent.rc > /home/$user/.rtorrent.rc

nginx_user=$(sed "s/<USERNAME>/${user^^}/g;s/<username>/$user/g" /usr/share/seedbox-templates/nginx-user)
sed -i "\$i $(echo $nginx_user)" /etc/nginx/sites-enabled/seedbox.conf

rm -f /home/$user/.session/rtorrent.lock
/etc/init.d/$user-rtorrent start

/etc/init.d/nginx restart
