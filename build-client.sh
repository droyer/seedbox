#!/bin/bash

rm -rf client/build
rm -rf server/wwwroot

cd client
npm run build

cp -r build ../server/wwwroot
