const express = require("express");
const { User } = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("../utils/jwt");
const HttpError = require("../utils/http-error");
const validate = require("../utils/validate");
const _ = require("lodash");

const router = express.Router();

router.post("/", async (req, res) => {
  await validate(req.body, "name");

  const { name, password } = req.body;

  const error = new HttpError("Invalid user or email.", 400);
  const user = await User.findOne({ name });
  if (!user) throw error;

  if (!user.password) return res.json({ userId: user.id });

  await validate(req.body, "password");

  const isValidPassword = await bcrypt.compare(password, user.password);
  if (!isValidPassword) throw error;

  const token = jwt.generate(user.id);

  res.json({ token, user: _.pick(user, ["name", "isAdmin"]) });
});

router.get("/can-register", async (req, res) => {
  const len = await User.countDocuments();
  res.json({ value: len === 0 });
});

module.exports = router;
