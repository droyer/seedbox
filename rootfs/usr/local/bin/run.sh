#!/bin/bash

mkdir -p /users
node ./scripts/get-users.js

if [ "$(ls -A /users)" ]; then
    for item in /users/*
    do
        user="$(basename -- $item)"
        pid=`cat $item`

        if ! id "$user" &>/dev/null; then
            create-user.sh $user $pid
        else
            rm -f /home/$user/.session/rtorrent.lock
            /etc/init.d/$user-rtorrent start
        fi
    done
fi

/etc/init.d/nginx start

npm start