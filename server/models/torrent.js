const mongoose = require("../utils/mongoose");
const validateUtility = require("../utils/validate");

const Torrent = mongoose.model("Torrent", {
  hash: {
    type: String,
    required: true,
  },
  name: {
    type: String,
  },
  size: {
    type: Number,
  },
  download: {
    type: Number,
  },
  sent: {
    type: Number,
  },
  reception: {
    type: Number,
  },
});

const validate = async (body) => {
  await validateUtility(
    body,
    "hash",
    "name",
    "size",
    "download",
    "sent",
    "ratio",
    "reception"
  );
};

module.exports = { Torrent, validate };
