const express = require("express");
require("express-async-errors");

const app = express();

require("./startup/routes")(app);

const start = async () => {
  try {
    if (!process.env.JWT_SECRET) throw new Error("JWT_SECRET is not defined.");

    await require("./startup/db")();

    const port = process.env.PORT || 5000;
    await app.listen(port);
    console.log(`Listening to ${port}`);
  } catch (err) {
    console.log(err.message);
  }
};

start();
