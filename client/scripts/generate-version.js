#!/usr/bin/env node

const { v1: uuid } = require("uuid");
const fs = require("fs").promises;

const appVersion = uuid();

const writeFile = async () => {
  try {
    await fs.writeFile("./public/version", appVersion);
  } catch (error) {
    console.log(error);
  }
};

writeFile();
