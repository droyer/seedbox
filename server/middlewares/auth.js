const HttpError = require("../utils/http-error");
const jwt = require("../utils/jwt");
const { User } = require("../models/user");

module.exports = async (req, res, next) => {
  const token = req.header("x-access-token") || req.query.token;
  if (!token) throw new HttpError("No access token provided", 401);

  try {
    const { id } = jwt.verify(token);
    const user = await User.findById(id);
    if (!user) throw new Error("Bad token provided", 400);
    req.user = user;
  } catch (err) {
    throw new HttpError(err.message, 400);
  }

  next && next();
};
