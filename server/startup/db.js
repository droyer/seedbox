const mongoose = require("mongoose");

module.exports = async () => {
  const db = process.env.DATABASE || "mongodb://localhost/seedbox";

  await mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  console.log(`Connected to ${db}`);
};
