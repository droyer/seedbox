const Mutex = require("async-mutex").Mutex;
const locks = new Map();

const getMutex = (id) => {
  if (!locks.has(id)) {
    locks.set(id, new Mutex());
  }
  return locks.get(id);
};

module.exports = getMutex;
