const HttpError = require("./http-error");

const validate = async (body, ...args) => {
  for (const item of args) {
    if (!body[item]) throw new HttpError(`'${item}' is required"`);
  }
};

module.exports = validate;
