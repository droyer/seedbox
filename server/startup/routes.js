const bodyParser = require("body-parser");
const cors = require("cors");
const auth = require("../routes/auth");
const torrents = require("../routes/torrents");
const users = require("../routes/users");
const data = require("../routes/data");
const error = require("../middlewares/error");

module.exports = (app) => {
  app.use(bodyParser.json());
  if (process.env.NODE_ENV === "development") {
    app.use(cors());
  }
  app.use("/api/auth", auth);
  app.use("/api/users", users);
  app.use("/api/torrents", torrents);
  app.use("/api/data", data);
  app.use(error);
};
